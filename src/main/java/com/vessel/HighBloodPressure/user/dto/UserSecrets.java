package com.vessel.HighBloodPressure.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="USER_SECRETS")
@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
public class UserSecrets {
    @Id
    @Column(name="USER_ID")
    private Integer userId;

    @Column(name="PASSWORD", nullable = false, unique = true)
    private String password;

    @Column(name="SALT", nullable = false)
    private String salt;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="USER_ID")
    private User user;

}
