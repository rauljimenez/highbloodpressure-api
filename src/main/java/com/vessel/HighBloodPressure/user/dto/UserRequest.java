package com.vessel.HighBloodPressure.user.dto;

import lombok.Value;

@Value
public class UserRequest {
    String username;
    String password;
}
