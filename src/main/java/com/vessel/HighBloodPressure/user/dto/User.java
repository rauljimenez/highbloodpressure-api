package com.vessel.HighBloodPressure.user.dto;

import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureReading;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "USERS")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Id
    @SequenceGenerator(
            name="users_user_id_seq",
            sequenceName="users_user_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator="users_user_id_seq"
    )
    @Column(name="USER_ID")
    private Integer userId;

    @Column(name="USERNAME", nullable = false, unique = true, length = 50)
    private String username;

    @Column(name="CREATED_ON", nullable = false)
    private Date createdOn;

    @OneToOne(mappedBy="user", cascade = CascadeType.ALL)
    private UserSecrets userSecrets;

    @OneToMany(mappedBy="user")
    private List<BloodPressureReading> bloodPressureReading;
}
