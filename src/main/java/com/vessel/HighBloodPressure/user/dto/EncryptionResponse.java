package com.vessel.HighBloodPressure.user.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class EncryptionResponse {
    String hashedPassword;
    String salt;
}
