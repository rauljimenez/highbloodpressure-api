package com.vessel.HighBloodPressure.user.controller;

import com.vessel.HighBloodPressure.user.dto.UserRequest;
import com.vessel.HighBloodPressure.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/create-user")
    public ResponseEntity<Void> createUser(@RequestBody UserRequest userRequest) {
        return userService.createUser(userRequest);
    }

    @PostMapping("/login")
    public ResponseEntity<Void> loginUser(@RequestBody UserRequest userRequest) {
        return userService.login(userRequest);
    }

}
