package com.vessel.HighBloodPressure.user.service;

import com.vessel.HighBloodPressure.user.dto.EncryptionResponse;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class EncryptingService {

    public EncryptionResponse hashAndSalt(String password) {
        String salt = BCrypt.gensalt();
        String saltedPassword = BCrypt.hashpw(password, salt);
        return EncryptionResponse.builder()
                .hashedPassword(saltedPassword)
                .salt(salt)
                .build();
    }


    public String hashAndSalt(String password, String salt) {
        return BCrypt.hashpw(password, salt);
    }
}
