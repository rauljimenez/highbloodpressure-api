package com.vessel.HighBloodPressure.user.service;

import com.vessel.HighBloodPressure.user.dto.EncryptionResponse;
import com.vessel.HighBloodPressure.user.dto.User;
import com.vessel.HighBloodPressure.user.dto.UserRequest;
import com.vessel.HighBloodPressure.user.dto.UserSecrets;
import com.vessel.HighBloodPressure.user.jpa.UserRepository;
import com.vessel.HighBloodPressure.user.jpa.UserSecretsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserSecretsRepository userSecretsRepository;

    @Autowired
    EncryptingService encryptingService;

    public ResponseEntity<Void> createUser(UserRequest userRequest) {
        User savedUser = userRepository.save(
                User.builder()
                        .username(userRequest.getUsername())
                        .createdOn(new Date())
                        .build()
        );

        EncryptionResponse encryptionResponse = encryptingService.hashAndSalt(userRequest.getPassword());
        UserSecrets savedUserSecrets = userSecretsRepository.save(
                UserSecrets.builder()
                    .userId(savedUser.getUserId())
                    .password(encryptionResponse.getHashedPassword())
                    .salt(encryptionResponse.getSalt())
                    .build()
        );

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Void> login(UserRequest userRequest) {
        UserSecrets userSecrets = userSecretsRepository.findById(
                userRepository.findByUsername(userRequest.getUsername()).get().getUserId()
        ).get();

        String userRequestHashedPassword = encryptingService.hashAndSalt(
                userRequest.getPassword(), userSecrets.getSalt());

        if (userSecrets.getPassword().equals(userRequestHashedPassword) &&
            userSecrets.getUser().getUsername().equals(userRequest.getUsername())
        )
            return ResponseEntity.ok().build();

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
