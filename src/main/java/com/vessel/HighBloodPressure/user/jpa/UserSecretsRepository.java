package com.vessel.HighBloodPressure.user.jpa;

import com.vessel.HighBloodPressure.user.dto.UserSecrets;
import org.springframework.data.repository.CrudRepository;


public interface UserSecretsRepository extends CrudRepository<UserSecrets, Integer> {
}
