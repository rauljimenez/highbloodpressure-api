package com.vessel.HighBloodPressure.bloodPressure.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class BloodPressureRequest {
    Integer userId;
    Integer systolic;
    Integer diastolic;
    Integer pulse;
}
