package com.vessel.HighBloodPressure.bloodPressure.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vessel.HighBloodPressure.user.dto.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name="BLOOD_PRESSURE_READING")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BloodPressureReading implements Serializable {
    @Id
    @SequenceGenerator(
            name="blood_pressure_reading_reading_id_seq",
            sequenceName="blood_pressure_reading_reading_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "blood_pressure_reading_reading_id_seq"
    )
    @Column(name = "READING_ID", nullable = false)
    private Integer readingId;

    @Column(name = "USER_ID", nullable = false)
    private Integer userId;

    @Column(name = "SYSTOLIC", nullable = false)
    private Integer systolic;

    @Column(name = "DIASTOLIC", nullable = false)
    private Integer diastolic;

    @Column(name = "PULSE", nullable = false)
    private Integer pulse;

    @Column(name = "CREATION_DATE", nullable = false)
    private Instant creationDate;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="USER_ID", insertable = false, updatable = false)
    private User user;
}
