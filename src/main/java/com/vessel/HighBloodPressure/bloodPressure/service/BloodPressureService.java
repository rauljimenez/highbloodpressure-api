package com.vessel.HighBloodPressure.bloodPressure.service;

import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureReading;
import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureRequest;
import com.vessel.HighBloodPressure.bloodPressure.jpa.BloodPressureRepository;
import com.vessel.HighBloodPressure.user.dto.User;
import com.vessel.HighBloodPressure.user.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class BloodPressureService {

    @Autowired
    BloodPressureRepository bloodPressureRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Void> saveReading(BloodPressureRequest bloodPressureRequest) {

        bloodPressureRepository.save(
                BloodPressureReading.builder()
                    .userId(bloodPressureRequest.getUserId())
                    .systolic(bloodPressureRequest.getSystolic())
                    .diastolic(bloodPressureRequest.getDiastolic())
                    .pulse(bloodPressureRequest.getPulse())
                    .creationDate(Instant.now())
                    .build()
        );

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<List<BloodPressureReading>> getReadingByUsername(String username) {
        User user = userRepository.findByUsername(username).get();
        List<BloodPressureReading> response = new ArrayList<>(bloodPressureRepository.findAllByUserId(user.getUserId()));
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Void> deleteReadingByUsernameAndReadingId(String username, String readingId) {
        User user = userRepository.findByUsername(username).get();
        BloodPressureReading bloodPressureReading = bloodPressureRepository.findById(Integer.parseInt(readingId)).get();
        if (bloodPressureReading.getUserId().equals(user.getUserId())) {
            bloodPressureRepository.deleteById(Integer.parseInt(readingId));
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
