package com.vessel.HighBloodPressure.bloodPressure.controller;

import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureReading;
import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureRequest;
import com.vessel.HighBloodPressure.bloodPressure.service.BloodPressureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BloodPressureController {

    @Autowired
    BloodPressureService bloodPressureService;

    @PostMapping("/blood-pressure")
    public ResponseEntity<Void> saveBloodPressure(@RequestBody BloodPressureRequest bloodPressureRequest) {
        return bloodPressureService.saveReading(bloodPressureRequest);
    }

    @DeleteMapping("/blood-pressure/user/{username}/reading/{readingId}")
    public ResponseEntity<Void> deleteByUsernameAndReadingId(@PathVariable String username, @PathVariable String readingId) {
        return bloodPressureService.deleteReadingByUsernameAndReadingId(username, readingId);
    }

    @GetMapping("/blood-pressure/user/{username}")
    public ResponseEntity<List<BloodPressureReading>> getByUsername(@PathVariable String username) {
        return bloodPressureService.getReadingByUsername(username);
    }

}
