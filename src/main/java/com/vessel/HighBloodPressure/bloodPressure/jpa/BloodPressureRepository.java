package com.vessel.HighBloodPressure.bloodPressure.jpa;

import com.vessel.HighBloodPressure.bloodPressure.dto.BloodPressureReading;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BloodPressureRepository extends CrudRepository<BloodPressureReading, Integer> {
    List<BloodPressureReading> findAllByUserId(Integer userId);
}
