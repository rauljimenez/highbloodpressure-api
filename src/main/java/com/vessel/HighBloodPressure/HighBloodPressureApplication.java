package com.vessel.HighBloodPressure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighBloodPressureApplication {

	public static void main(String[] args) {
		SpringApplication.run(HighBloodPressureApplication.class, args);
	}

}
